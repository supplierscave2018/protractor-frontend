var Admindashboard = require(__dirname + '/../../../../dashboard/admin-dashboard/admin-dashboard.page.js');
var Adminsupplier = require(__dirname + '/add-supplier.page.js');


describe("Add supplier Functionality", function () {

        it("Click on Suppliers", function () {
                Admindashboard.clickOnSuppliers();
        })

        it("Click on Add for adding  Supplier", function () {
                Adminsupplier.clickOnAddSupplier();
        })

        it("Enter Company Details", function () {
                Adminsupplier.enterCompanyDetails('Milton');
        })

        it("Clear Web Address of the company", function () {
                Adminsupplier.clearWebAddress();
        })

        it("Enter Web Addresss for the company", function () {
                Adminsupplier.enterWebAddress('www.milton.co.in');
        })

        // it("Select Logo for the company",function(){
        //     Adminsupplier.selectLogo();
        // })

        it("Upload Logo for the company", function () {
                Adminsupplier.uploadLogo();
        })

        it("Select Establishment type for the company", function () {
                Adminsupplier.selectEstablishmentType('Government');
        })

        it("Select Company Type", function () {
                Adminsupplier.companyType('Large');
        })

        it("Select Supplier Checkbox", function () {
                Adminsupplier.selectSupplierCheckbox();
        })

        it("Select Buyer Checkbox", function () {
                Adminsupplier.selectBuyerCheckbox();
        })

        // it("Select Company Profile",function(){
        //     Adminsupplier.clickCompanyProfile('Trader');
        // })

        it("Upload Attach incorporation document / License", function () {
                Adminsupplier.documentLicense();
        })

        it("Clear Employees for the company", function () {
                Adminsupplier.clearEmployees();
        })

        it("Enter Number of Employees for the company", function () {
                Adminsupplier.employees('25000');
        })

        // it("Enter details in About Us", function () {
        //         Adminsupplier.aboutUs('Supplierscave.com is an online platform to connect suppliers and buyers of industrial products and services.');
        // })

        it("Upload about us images ", function () {
                Adminsupplier.aboutUsImage();
        })

        it("Add Catalog images", function () {
                Adminsupplier.catalogImages();
        })

        it("Click on Add Address", function () {
                Adminsupplier.addAddress();
        })

        it("Add name for the Address", function () {
                Adminsupplier.nameOfAddress('Shanthi Nilayam');
        })

        it("Address-1 for the company", function () {
                Adminsupplier.address1('HSR Layout');
        })

        it("Address-2 for the company", function () {
                Adminsupplier.address2('Near BDA Complex');
        })

        it("City", function () {
                Adminsupplier.city('Bangalore');
        })

        it("State", function () {
                Adminsupplier.state('Karnataka');
        })

        it("Country", function () {
                Adminsupplier.country('India');
        })

        it("Google Maps URL", function () {
                Adminsupplier.googleMapsUrl('https://www.google.co.in/maps/@12.9183494,77.6364297,15z?hl=en');
        })

        it("Country code-1", function () {
                Adminsupplier.countryCode1('91');
        })

        it("Phone Number-1", function () {
                Adminsupplier.phoneNumber1('8523698521');
        })

        it("Country Code-2", function () {
                Adminsupplier.countryCode2('91');
        })

        it("Phone Number-2", function () {
                Adminsupplier.phoneNumber2('9676902569');
        })

        it("Email id-1", function () {
                Adminsupplier.emailId1('power893892@gmail.com');
        })

        it("Email id-2", function () {
                Adminsupplier.emailId2('power902876@gmail.com');
        })

        it("Facebook URL", function () {
                Adminsupplier.facebookUrl('https://www.facebook.com/');
        })

        it("Twitter URL", function () {
                Adminsupplier.twitterUrl('https://twitter.com/');
        })

        it("LinkedIn URL", function () {
                Adminsupplier.linkedInUrl('https://www.linkedin.com');
        })

        it("Youtube URL", function () {
                Adminsupplier.youtubeUrl('https://www.youtube.com/');
        })

        it("Save company Information", function () {
                Adminsupplier.saveCompanyInfo();
        })

})