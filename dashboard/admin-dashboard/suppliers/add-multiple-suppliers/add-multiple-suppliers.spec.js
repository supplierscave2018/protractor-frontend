var Addmultiplesuppliers = require(__dirname + '/add-multiple-suppliers.page.js');
var Admindashboard = require(__dirname + '/../../../../dashboard/admin-dashboard/admin-dashboard.page.js');


describe("Add Multiple Suppliers Functionality", function () {

    it("Click on Suppliers", function () {
        Admindashboard.clickOnSuppliers();
    })

    it("Click on Add Multiple Suppliers", function () {
        Addmultiplesuppliers.clickOnAddMultipleSuppliers();
    })

    it("Upload Supplier CSV  File",function(){
        Addmultiplesuppliers.uploadSupplierFile();
    })

    it("Click on Save Changes for multiple supplier upload",function(){
        Addmultiplesuppliers.multipleSupplierSaveChanges();
    })

})