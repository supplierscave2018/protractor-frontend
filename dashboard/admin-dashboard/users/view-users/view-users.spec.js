var Adminviewusers = require(__dirname + '/view-users.page.js');

describe("User Functionality", function () {

    // it("Click on Users in Admin Dashboard",function(){
    //     Admindashboard.clickOnAdminUsers();
    // })

    it("Click on Role-Type", function () {
        Adminviewusers.clickOnRoleType('Manufacturer');
    })

    it("Select supplier", function () {
        Adminviewusers.selectSupplier('ITC PVT LTD');
    })

    it("Search for users by firstname", function () {
        Adminviewusers.searchByFirstName('Supplier');
    })

    it("Search for admin by Last Name", function () {
        Adminviewusers.searchByLastName("Sreekanth")
    })

    it("Apply Filters", function () {
        Adminviewusers.applyFilters();
    })

    it("Clear Filters what you have selected", function () {
        Adminviewusers.clear();
    })

    it("Select Admin checkbox", function () {
        Adminviewusers.selectAdminCheckbox();
    })

    it("Select Buyer Checkbox", function () {
        Adminviewusers.selectBuyerCheckbox();
    })

    it("Select Supplier Checkbox", function () {
        Adminviewusers.selectSupplierCheckbox();
    })

    it("select Admin Supplier Checkbox", function () {
        Adminviewusers.selectAdminSupplierCheckbox();
    })

    it("Select Staff checkbox", function () {
        Adminviewusers.selectIsStaffCheckbox();
    })

    it("Apply Filters", function () {
        Adminviewusers.applyFilters();
    })








})