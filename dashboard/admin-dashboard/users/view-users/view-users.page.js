var Adminviewusers = function () {

    this.clickOnRoleType = function (data) {
        browser.sleep(1000);
        element(by.id('select-role-type')).click();
        browser.sleep(1000);
        element(by.id('selectAll')).click();;
        browser.sleep(1000);
    }

    this.selectSupplier = function (data) {
        browser.sleep(1000);
        element(by.id('selectSuppliervalue')).click();
        browser.sleep(1000);
        element(by.id('selectSuppliervalue')).sendKeys(data);
        browser.sleep(1000);
    }

    this.applyFilters = function () {
        browser.sleep(5000);
        element(by.id('apply-filters')).click();
        browser.sleep(5000);
    }

    this.clear = function () {
        browser.sleep(10000);
        element(by.id('clear')).click();
        browser.sleep(10000);
    }

    this.searchByFirstName = function (data) {
        element(by.id('searchFirstName')).sendKeys(data);
    }

    this.searchByLastName=function(data){
        element(by.id('searchLastName')).sendKeys(data);
    }

    this.selectAdminCheckbox = function () {
        browser.sleep(2000);
        $('[id="is_superuser"]').click();
        browser.sleep(2000);
    }

    this.selectBuyerCheckbox = function () {
        browser.sleep(1000);
        $('[id="is_buyer"]').click();
        browser.sleep(1000);
    }

    this.selectSupplierCheckbox = function () {
        browser.sleep(1000);
        $('[id="is_seller"]').click();
        browser.sleep(1000);
    }

    this.selectAdminSupplierCheckbox = function () {
        browser.sleep(1000);
        $('[id="is_admin_supplier"]').click();
        browser.sleep(1000);
    }

    this.selectIsStaffCheckbox = function () {
        browser.sleep(1000);
        $('[id="is_staff"]').click();
        browser.sleep(1000);
    }





}
module.exports = new Adminviewusers;