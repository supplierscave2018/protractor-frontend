var Admindashboard= require(__dirname+'/../../../../dashboard/admin-dashboard/admin-dashboard.page.js');
// var Viewinventoriesrental = require(__dirname+'/../../../../dashboard/admin-dashboard/inventories-rental/view-inventories-rental/view-inventories-rental.page.js');
var Editinventoriesrental = require(__dirname+'/edit-inventories-rental.page.js');

describe("Edit Inventories and Rental",function(){

    it("Click on Inventories and Rental",function(){
        Admindashboard.clickOnInventoriesAndRental();
    })

    it("Click on Edit",function(){
        Editinventoriesrental.clickOnEdit();
    })

    it("Select Category", function () {
        Editinventoriesrental.selectSubCategory();
    });

    it("Select sub Category",function(){
        Editinventoriesrental.selectsSelectedSubCategory();
    })

    // it("Select secondary sub category",function(){
    //     Editinventoriesrental.selectSelectedSecondarySubCategory();
    // })

    // it("Select country",function(){
    //     Editinventoriesrental.selectCountries();
    // })

    // it("Select State",function(){
    //     Editinventoriesrental.selectState();
    // })

    // it("Apply Filters",function(){
    //     Editinventoriesrental.applyFilters();
    // })

    // it("Clear Filters",function(){
    //     Editinventoriesrental.clear();
    // })


})