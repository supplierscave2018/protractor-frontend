var Admindashboard = function () {

    this.clickOnAdminUsers = function () {
        element(by.id("admin-user")).click();
    }

    this.clickOnSuppliers = function () {
        browser.sleep(1000);
        element(by.id("admin-supplier")).click();
        browser.sleep(1000);     
    }

    this.clickOnInventoriesAndRental = function () {
        element(by.id("admin-inventory")).click();
    }

    this.clickOnProductAndServicesInDirectory = function () {
        element(by.id("admin-directory")).click();
    
    }

    this.clickOSupplierLocationAndContactDetails = function () {
        element(by.id("admin-contact")).click();
    }

    this.clickOnManufacturers = function () {
        element(by.id("admin-manufacturer")).click();
    }

    this.clickOnIndustries = function () {
        element(by.id("admin-industry")).click();
    }

    this.clickOnParentCategories = function () {
        element(by.id("admin-parent-category")).click();
    }

    this.clickOncategory = function () {
        element(by.id("admin-category")).click();
    }

    this.clickOnLocation = function () {
        element(by.id("admin-city")).click();
    }

    this.clickOnCurrency = function () {
        element(by.id("admin-currency")).click();
        browser.sleep(10000);
    }

    this.clickOnContent = function () {
        element(by.id("admin-content")).click();
        browser.sleep(10000);
    }

    // this.clickOnContent = function () {
    //     expect(element(by.tagName('a')).getText()).toBe('Content').click();

    //    // element(by.tagName('Content')).click();
    //     browser.sleep(5000);
    // }


    this.clickOnFaqInContent = function () {
        element(by.id('admin-content')).$$('li');
    }



    this.clickOnNewsletter = function () {
        element(by.id("admin-newsletter")).click();
        browser.sleep(10000);
    }

    this.clickOnSiteLogo=function(){
        element(by.id('site-logo')).click();
    }


    this.clickActions = function () {
        browser.executeScript('window.scrollTo(0,10000);')
    }



    // this.content=function(){
    //     element(by.css('.dropdown button arrow-only')).click();
    //     // browser.sleep(5000);
    // }  


}


module.exports = new Admindashboard;