var supplierDashboard = function () {

    this.clickSummary = function () {
        element(by.id("supplier-summary")).click();
    }
    this.clickInventoriesAndRental = function () {
        element(by.id("supplier-inventory")).click();
    }

    this.clickProductAndServiceInDirectory=function(){
        element(by.id("supplier-directory")).click();     
    }

    this.clickTeam=function(){
        element(by.id("supplier-team")).click();
    }

    this.clickOnSiteLogo=function(){
        element(by.id('site-logo')).click();
    }

}
module.exports = new supplierDashboard;