var Getstarted = require(__dirname + '/../../getstarted/getstarted.page.js');
var Homepagelogin = require(__dirname + '/../../homepagelogin/homepagelogin.page.js');
var Dashboard= require(__dirname+'/../dashboard.page.js');
var Supplierdashboard=require(__dirname+'/supplier-dashboard.page.js');
var Home=require(__dirname+'/../../homepage/homepage.page.js');

describe("Suppliers Dashboard Full Functionality", function () {

    it("Getstarted page", function () {
        Getstarted.get();
    })

    it("Enter Supplier Email", function () {
        Homepagelogin.userName("livelikeme1986+protractorsupplieradmin@gmail.com");
    })

    it("Enter Supplier Password", function () {
        Homepagelogin.userPwd("supplierscave");
    })

    it("Click on Login", function () {
        Homepagelogin.userLogin();
    })

    it("clicks on actions in application", function () {
        Dashboard.clickActions();
    })

    it("Click on Suppliers Dashboard", function () {
        Dashboard.clickSuppliersDashboard();
    })

    // it("Click on Summary",function(){
    //     Supplierdashboard.clickSummary();
    // })

    it("Click on Inventory and Rental",function(){
        Supplierdashboard.clickInventoriesAndRental();
    })

    it("Click on Product and Services in directory",function(){
        Supplierdashboard.clickProductAndServiceInDirectory();
    })

    it("Click on Team ",function(){
        Supplierdashboard.clickTeam();
    })

    it("Click on Suppliers Logo",function(){
        Supplierdashboard.clickOnSiteLogo();
    })

    it("Logout form Buyer Dashboard",function(){
        Dashboard.clickLogout();
    })


    

})