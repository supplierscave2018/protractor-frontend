var Dashboard = require(__dirname + '/dashboard.page.js');
var Getstarted = require(__dirname+'/../getstarted/getstarted.page.js');
var Homepagelogin = require(__dirname+'/../homepagelogin/homepagelogin.page.js');
var Homepage = require(__dirname+'/../homepage/homepage.page.js');
var Myaccount = require('../myaccount/myaccount.page.js');

// var getstarted = new Getstarted();
// var dashboard = new Dash();
// var home= new Home();
// var login=new Login();
// var myacc=new Myaccunt();

describe("Dashboard Page", function () {

    it("Opens Supplierscave Application ", function () {
        Getstarted.get();
    })

    it("Enter User Name", function () {
        Homepagelogin.userName('livelikeme1986+supplieradmin06112018@gmail.com');
    })

    it("Enter Password", function () {
        Homepagelogin.userPwd('supplierscave');
    })

    it("Click on Login", function () {
        Homepagelogin.userLogin();
    })

    // it("Myaccount on the dashboard", function () {
    //     Myaccount.myAcc();
    // })

    // it("clicks on actions in application", function () {
    //     Dashboard.clickActions();
    // })

    it("Click on Suppliers Logo", function () {
        Homepage.clickOnSiteLogo();
    })

    it("Click on Logout", function () {
        Dashboard.clickLogout();
    })



})