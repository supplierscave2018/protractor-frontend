var Getstarted = require(__dirname + '/../../../getstarted/getstarted.page.js');
var Homepagelogin = require(__dirname + '/../../../homepagelogin/homepagelogin.page.js')
var Dashboard = require(__dirname + '/../../../dashboard/dashboard.page.js')
var Managecompany = require(__dirname + '/manage-company.page.js');

describe("Manage Company functionality for supplier Admmin",function(){

    // it("Login into Supplier Admin Dashboard ", function () {
    //     Getstarted.get();
    // });

    // it("Enter Username for Supplier Admin", function () {
    //     Homepagelogin.userName('livelikeme1986+supplieradmin11@gmail.com');
    // })

    // it("Enter User Password for Supplier Admin", function () {
    //     Homepagelogin.userPwd('supplierscave');
    // })

    // it("Click on Login", function () {
    //     Homepagelogin.userLogin();
    // })

    // it("MouseOver to  Supplier Admin ",function(){
    //     Dashboard.clickActions();
    // })

    // it("Click on Supplier Admin ",function(){
    //     Dashboard.clickAdminSupplierDashboard();
    // })

    it("Click on Manage Company",function(){
        Managecompany.clickManageCompany();
    })

    it("Click on Edit Company Details",function(){
        Managecompany.clickEditCompanyDetails();
    })

    it("Clear Company Detials",function(){
        Managecompany.clearCompanyDetails();

    })

    it("Enter Company Details",function(){
        Managecompany.enterCompanyDetails('Verizon PVT LTD');
    })

    it("Clear Web Address of the company",function(){
        Managecompany.clearWebAddress();
    })

    it("Enter Web Addresss for the company",function(){
        Managecompany.enterWebAddress('www.verizon.co.in');
    })

    // it("Select Logo for the company",function(){
    //     Managecompany.selectLogo();
    // })

    it("Upload Logo for the company",function(){
        Managecompany.uploadLogo();
    })

    it("Select Establishment type for the company",function(){
        Managecompany.selectEstablishmentType('Government');
    })

    it("Select Company Type",function(){
        Managecompany.companyType('Large');
    })

    it("Select Supplier Checkbox",function(){
        Managecompany.selectSupplierCheckbox();
    })

    it("Select Buyer Checkbox",function(){
        Managecompany.selectBuyerCheckbox();
    })

    // it("Select Company Profile",function(){
    //     Managecompany.clickCompanyProfile('Trader');
    // })

    it("Upload Attach incorporation document / License",function(){
        Managecompany.documentLicense();
    })

    it("Clear Employees for the company",function(){
        Managecompany.clearEmployees();
    })

    it("Enter Number of Employees for the company",function(){
        Managecompany.employees('25000');
    })

    // it("Enter details in About Us",function(){
    //     Managecompany.aboutUs('Supplierscave.com is an online platform to connect suppliers and buyers of industrial products and services.');
    // })

    it("Upload about us images ",function(){
        Managecompany.aboutUsImage();
    })

    it("Click on Add Address",function(){
        Managecompany.addAddress();
    })

    it("Add name for the Address",function(){
        Managecompany.nameOfAddress('Shanthi Nilayam');
    })

    it("Address-1 for the company",function(){
        Managecompany.address1('HSR Layout');
    })

    it("Address-2 for the company",function(){
        Managecompany.address2('Near BDA Complex');
    })

    it("City",function(){
        Managecompany.city('Bangalore');
    })

    it("State",function(){
        Managecompany.state('Karnataka');
    })

    it("Country",function(){
        Managecompany.country('India');
    })

    it("Google Maps URL",function(){
        Managecompany.googleMapsUrl('https://www.google.co.in/maps/@12.9183494,77.6364297,15z?hl=en');       
    })

    it("Country code-1",function(){
        Managecompany.countryCode1('91');
    })

    it("Phone Number-1",function(){
        Managecompany.phoneNumber1('8523698521');
    })

    it("Country Code-2",function(){
        Managecompany.countryCode2('91');
    })

    it("Phone Number-2",function(){
        Managecompany.phoneNumber2('9676902569');
    })

    it("Email id-1",function(){
        Managecompany.emailId1('power893892@gmail.com');
    })

    it("Email id-2",function(){
        Managecompany.emailId2('power902876@gmail.com');
    })

    it("Facebook URL",function(){
        Managecompany.facebookUrl('https://www.facebook.com/');
    })

    it("Twitter URL",function(){
        Managecompany.twitterUrl('https://twitter.com/');
    })

    it("LinkedIn URL",function(){
        Managecompany.linkedInUrl('https://www.linkedin.com');
    })

    it("Youtube URL",function(){
        Managecompany.youtubeUrl('https://www.youtube.com/');
    })

    it("Save company Information",function(){
        Managecompany.saveCompanyInfo();
    })

    // it("Click on Preview Company Page",function(){
    //     Managecompany.previewCompanyPage();       
    // })



})
