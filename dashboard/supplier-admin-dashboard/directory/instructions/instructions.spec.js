var Adddirectory=require(__dirname+'/../add-directory/add-directory.page.js');
var Instructions = require(__dirname + '/instructions.page.js')

describe("Directory Instructions Functionality",function(){

    it("Click on Product and Services",function(){
        Adddirectory.clickOnProductAndServicesInDirectory();
    })
    
    it("Click on Instructions",function(){
        Instructions.clickOnInstructions();
    })

    it("Click Ok",function(){
        Instructions.clickOnOk();
    })
})