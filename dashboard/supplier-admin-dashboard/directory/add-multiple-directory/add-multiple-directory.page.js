var Addmultipledirectory = function () {

    this.clickOnProductAndServicesInDirectory = function () {
        element(by.id('supplier-directory')).click();
    }

    this.clickOnMultipleProduct = function () {
        element(by.id('add-multiple-products')).click();
    }

    this.selectIndustryType=function(){
        element(by.id('selected-industry')).click();
        browser.sleep(1000);
        element(by.xpath('//span[text()="Building Construciton"]')).click();
        element(by.xpath('//span[text()="Chemicals"]')).click();
    }

    this.downloadDirectoryTemplate=function(){
        element(by.id('download-directory-template')).click();
    }

    this.uploadImages = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-multiple-directory/directory.jpeg';
        absolutePath = path.resolve(__dirname, fileToUpload);
        // // element(by.class('ng-isolate-scope')).sendKeys(absolutePath);
        // element(by.css('input[type="file"]')).sendKeys(absolutePath);    
        //    element(by.xpath("//input[@ng-multi-file-model='productImages']")).sendKeys(absolutePath);
        browser.sleep(1000);
        element(by.id('multiple-directory-product-image')).sendKeys(absolutePath);
        browser.sleep(1000);

        // element(by.css('input[type="file"]')).click();
        // element(by.id('multiple-directory-upload-image')).click();
        //    browser.sleep(15000);
    };

    this.uploadImages1=function(){
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-multiple-directory/directory-1.jpeg';
        absolutePath = path.resolve(__dirname,fileToUpload);
        browser.sleep(1000);
        element(by.id('multiple-directory-product-image')).sendKeys(absolutePath);
        browser.sleep(1000);
    }

    this.uploadImages2=function(){
        var path= require('path');
        var fileToUpload ='/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-multiple-directory/directory-2.jpeg';
        absolutePath=path.resolve(__dirname,fileToUpload);
        browser.sleep(1000);
        element(by.id('multiple-directory-product-image')).sendKeys(absolutePath);
        browser.sleep(1000);
    }

    this.uploadDocuments = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-multiple-directory/Directory_Upload_Format_Admin.csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        // // element(by.class('ng-isolate-scope')).sendKeys(absolutePath);
        // element(by.css('input[type="file"]')).sendKeys(absolutePath);    
        // element(by.xpath("//input[@ng-multi-file-model='fileAttachments']")).sendKeys(absolutePath);
        browser.sleep(1000);

        element(by.id('multiple-directory-documents-attachments')).sendKeys(absolutePath);
        browser.sleep(1000);

        // element(by.css('input[type="file"]')).click();
        // element(by.id('multiple-directory-upload-files')).click();
    };

    this.uploadDocuments1=function(){
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-multiple-directory/Directory_Upload_Format_Admin-1.csv';
        absolutePath=path.resolve(__dirname,fileToUpload);
        browser.sleep(1000);
        element(by.id('multiple-directory-documents-attachments')).sendKeys(absolutePath);
        browser.sleep(1000);
    }

    this.uploadFiles = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-multiple-directory/Valmark.csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        // // element(by.class('ng-isolate-scope')).sendKeys(absolutePath);
        // element(by.css('input[type="file"]')).sendKeys(absolutePath);    
        // element(by.xpath("//input[@ng-multi-file-model='fileAttachments']")).sendKeys(absolutePath);
        browser.sleep(1000);
        element(by.id('multiple-directory-file-attachement')).sendKeys(absolutePath);
        browser.sleep(1000);
        // element(by.css('input[type="file"]')).click();
        // element(by.id('multiple-directory-import-data')).click();
    };

    this.saveMultipleDirectory = function () {
        browser.sleep(2000);
        element(by.id('save-multiple-directories')).click();
        browser.sleep(2000);
    };







}
module.exports = new Addmultipledirectory;