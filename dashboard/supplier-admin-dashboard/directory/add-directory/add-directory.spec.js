// var Getstarted = require(__dirname + '/../../../getstarted/getstarted.page.js');
// var Homepagelogin = require(__dirname + '/../../../homepagelogin/homepagelogin.page.js')
// var Dashboard = require(__dirname + '/../../../dashboard/dashboard.page.js')
var Adddirectory = require(__dirname + '/add-directory.page.js');

describe('Add directory as a supplier Admin', function () {

    // it("Open supplierscave application", function () {
    //     Getstarted.get();
    // })

    // it("Enter Username for Supplier Admin", function () {
    //     Homepagelogin.userName('livelikeme1986+supplieradmin4@gmail.com');
    // })

    // it("Enter User Password for Supplier Admin", function () {
    //     Homepagelogin.userPwd('supplierscave');
    // })

    // it("Click on Login", function () {
    //     Homepagelogin.userLogin();
    // })

    // it("MouseOver to  Supplier Admin ", function () {
    //     Dashboard.clickActions();
    // })

    // it("Click on Supplier Admin ", function () {
    //     Dashboard.clickAdminSupplierDashboard();
    // })

    it("Click On Product & Services in directory ", function () {
        Adddirectory.clickOnProductAndServicesInDirectory();
    })

    it("Click on Add Directory", function () {
        Adddirectory.addDirectory();
    })

    it("Add Title for the product", function () {
        Adddirectory.addTitle('Longwave Flashwave wave');
    })

    it("Add Description for the product", function () {
        Adddirectory.addDescription('The Longwave Flashwave 4100 ES Micro Packet Optical Networking Platform (Packet ONP) is ideal for Ethernet and TDM access service delivery and aggregation over networks up to 10 Gbps. For high concentrations of DS1, DS3, SONET or Ethernet services over OC-3/12/48/192, or when footprint is at a premium, the FLASHWAVE 4100 ES Micro Packet ONP is a compact, modular solution using a 2RU chassis. This system can operate as a standalone UPSR network element or terminal SONET shelf.');
    })


    it("Select Category of the directory", function () {
        Adddirectory.selectCategory('Building Material');
    })

    it("Select Sub-Category of the directory", function () {
        Adddirectory.selectSubCategory('Interior');
    })

    it("Select Secondary Sub Category of the directory", function () {
        Adddirectory.selectSecondarySubCategory('Structural');
    })

    it("Select Tertiary Sub Category", function () {
        Adddirectory.selectTertiarySubCategory('Edge Trim');
    })

    it("Material of the product", function () {
        Adddirectory.prodcutMaterial('Plastic');
    })

    it("Grade of the product", function () {
        Adddirectory.productGrade('B');
    })

    it("Standard of the product", function () {
        Adddirectory.productStandard('ISO 2000:2020');
    })

    it("Add country for the product", function () {
        Adddirectory.productCountry('India');
    })

    it("Add State for the product", function () {
        Adddirectory.productState('Maharashtra');
    })

    it("Add City for the product", function () {
        Adddirectory.productCity('Nasik');
    })

    it("Add Manufacturer of the product", function () {
        Adddirectory.productManufacturer('MTR Services');
    })

    it("Select Industry of the Product", function () {
        Adddirectory.productIndustry();
    })

    it("Add Remarks for the product", function () {
        Adddirectory.productRemarks('In marketing, a product is anything that can be offered to a market that might satisfy a want or need.[1] In retailing, products are called merchandise. In manufacturing, products are bought as raw materials and sold as finished goods. A service is another common product type');
    })

    it("Upload Image for the product", function () {
        Adddirectory.uploadImages();
    })

    it("Upload Image-1 for the product", function () {
        Adddirectory.uploadImages1();
    })

    it("Upload Image-2 for the product", function () {
        Adddirectory.uploadImages2();
    })

    it("Upload Document for the product", function () {
        Adddirectory.uploadDocuments();
    })


    it("Upload Document1 for the product", function () {
        Adddirectory.uploadDocuments1();
    })

    it("Upload Document2 for the product", function () {
        Adddirectory.uploadDocuments2();
    })

    it("Save Product", function () {
        Adddirectory.saveDirectory();
    })


})