var Addmultipleinventories=require(__dirname+'/add-multiple-inventories.page.js');

describe("Add Multiple Inventories",function(){


    it("Click on Add multiple Inventories",function(){
        Addmultipleinventories.addMultipleInventories();
    })

    it("Select Industry Type while adding multiple inventories",function(){
        Addmultipleinventories.selectIndustryType();
    })

    it("Select Item Type while adding multiple Inventories",function(){
        Addmultipleinventories.selectItemType();
    })

    it("Click on Download Inventory Template",function(){
        Addmultipleinventories.downloadInventoryTemplate();
    })

    it("Click on Download Rental Template",function(){
        Addmultipleinventories.downloadRentalTemplate();
    })

    it("Upload Inventory Image for multiple inventories",function(){
        Addmultipleinventories.uploadImages();
    })

    it("Upload Inventory Image1 for multiple inventories",function(){
        Addmultipleinventories.uploadImages1();
    })

    it("Upload Inventory Image2 for multiple inventories",function(){
        Addmultipleinventories.uploadImages2();
    })

    it("Add Documents for mulitple inventories",function(){
        Addmultipleinventories.uploadDocuments();
    })

    it("Add Documents1 for multiple inventories",function(){
        Addmultipleinventories.uploadDocuments1();
    })

    it("Add Inventory for multiple inventories",function(){
        Addmultipleinventories.uploadInventory();
    })

    it("Save Multiple Inventories",function(){
        Addmultipleinventories.saveMultipleInventories();
    })

})
