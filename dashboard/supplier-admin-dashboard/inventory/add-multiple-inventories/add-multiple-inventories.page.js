var Addmultipleinventories = function () {

    this.selectIndustryType = function(){
        element(by.id('selected-industry')).click();
        browser.sleep(1000);
        // element(by.xpath('//span[text()="Check All"]')).click();
        element(by.xpath('//span[text()="Building Construciton"]')).click();
        element(by.xpath('//span[text()="Oil & Gas"]')).click();
    }

    this.selectItemType=function(){
        element(by.id('selected-item-type')).click();
        browser.sleep(1000);
        element(by.xpath('//*[@id="selected-item-type"]/option[3]')).click();
    }

    this.addMultipleInventories = function () {
        element(by.id('add-multiple-inventories')).click();
    }

    this.downloadInventoryTemplate=function(){
        element(by.id("download-inventory-template")).click();
    }

    this.downloadRentalTemplate=function(){
        element(by.id("download-rental-template")).click();
    }

    this.uploadImages = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-multiple-inventories/inventory.jpeg';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(1000);
        element(by.id('multiple-inventory-product-image')).sendKeys(absolutePath);
        browser.sleep(1000);
    };

    this.uploadImages1 = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-multiple-inventories/inventory-2.jpeg';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(1000);
        element(by.id('multiple-inventory-product-image')).sendKeys(absolutePath);
        browser.sleep(1000);
    };

    this.uploadImages2 = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-multiple-inventories/inventory-1.jpeg';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(1000);
        element(by.id('multiple-inventory-product-image')).sendKeys(absolutePath);
        browser.sleep(1000);
    };

    this.uploadDocuments = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-multiple-inventories/GeneralCableKSA.csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(1000);
        element(by.id('multiple-inventory-documents-attachments')).sendKeys(absolutePath);
        browser.sleep(1000);
    };


    this.uploadDocuments1 = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-multiple-inventories/Inventory_Upload_Format_Admin.csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(1000);
        element(by.id('multiple-inventory-documents-attachments')).sendKeys(absolutePath);
        browser.sleep(1000);
    };

    this.uploadInventory = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-multiple-inventories/test (2).csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(1000);
        element(by.id('multiple-inventory-file-attachments')).sendKeys(absolutePath);
        browser.sleep(1000);
    };

    this.saveMultipleInventories = function () {
        browser.sleep(2000);
        element(by.id('save-mulitple-inventories')).click();
        browser.sleep(2000);
    }

}
module.exports = new Addmultipleinventories;