
    // var Getstarted = require(__dirname + '/../../getstarted/getstarted.page.js');
    // var Homepagelogin = require(__dirname + '/../../homepagelogin/homepagelogin.page.js')
    // var Dashboard = require(__dirname+'/../../dashboard/dashboard.page.js')
    var Addinventory = require(__dirname+'/add-inventory.page.js')
    
    describe('Add Inventory as a supplier Admin', function () {
       
    it("Click On to Add Inventory",function(){
        Addinventory.addInventory();
    })

    // it("Click on to Add Multiple Inventories",function(){
    //        Supplieradmindashboard.addMulitpleInventory();
    // })

    it("Add Title to Inventory",function(){
        Addinventory.addTitleToInventory('Cherokee');
    })

    it("Add Description to Inventory",function(){
        Addinventory.addDescriptionToInventory('Cherokee is the USA Company');
    })

    it("Add Category",function(){
        Addinventory.selectInventoryCategory('Chemicals');
    })

    it("Add Sub-Category",function(){
        Addinventory.selectSubInventory('Chemicals');
    })

    it("Add Secondary Sub-directory",function(){
        Addinventory.selectSecondarySubCategory('Organic Pigments:');
    })  

    it("Enter Total Quantity of the inventory",function(){
        Addinventory.totalQuantity(60);
    })

    it("Enter Quantity sold of the inventory",function(){
        Addinventory.quantitySold(30);
    })

    it("Enter Availbale Quantity of the inventory",function(){
        Addinventory.availableQuantity(40)
    })

    it("Enter Materail of the inventory",function(){
        Addinventory.addMaterial('Fabric');
    })

    it("Enter Grade of the inventory",function(){
        Addinventory.addGrade('A');
    })

    it("Enter Standard of the Inventory",function(){
        Addinventory.addStandard('ISO 9001:2000');
    })

    it("Add Country",function(){
        Addinventory.selectCountry('United Arab Emirates');
    })

    it("Add State",function(){
        Addinventory.selectState('Dubai')
    })

    it("Add City",function(){
        Addinventory.selectCity('Dubai');
    })

    it("Add Manufacture of the inventory",function(){
        Addinventory.addManufacturer('Lion LTD')
    })

    it("Select Industry",function(){
        Addinventory.selectIndustry();
    })

    it("Enter Remarks for the inventory",function(){
        Addinventory.enterRemarks('Remarks for the inventory products');
    })

    it("Upload Inventory Image",function(){
        Addinventory.uploadImages();
    })

    it("Upload Inventory Image1",function(){
        Addinventory.uploadImages1();
    })

    it("Upload Inventory Image2",function(){
        Addinventory.uploadImages2();
    })

    it("Upload Inventory Document",function(){
        Addinventory.uploadDocuments();
    }) 

    it("Upload Inventory Document1",function(){
        Addinventory.uploadDocuments1();
    })

    it("Click on Save",function(){
        Addinventory.saveInventory();
    })

});