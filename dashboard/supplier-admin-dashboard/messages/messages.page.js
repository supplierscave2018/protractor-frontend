var Messages = function(){
    this.clickOnMessages=function(){
        element(by.id('messages')).click();
    }

    this.clickOnReceivedMessages=function(){
        element(by.id('received-messages')).click();
    }

    this.clickOnSentMessages=function(){
        browser.sleep(2000);
        element(by.id('sent-messages')).click();
        browser.sleep(2000);
    }

}
module.exports=new Messages;