var Dashboard = function () {

    this.clickLogout = function () {
        element(by.id('header-logout')).click();
    }

    this.clickActions = function () {
        browser.actions().mouseMove(element(by.id("dashboards"))).perform();
    };

    this.clickAdminDashboard = function () {
        element(by.id("admin-dashboard")).click();
    }

    this.clickSupplierAdminDashboard=function(){
        browser.sleep(2000);
        element(by.id("supplier-dashboard")).click();
        browser.sleep(2000);
    }

    this.clickSuppliersDashboard = function () {
        element(by.id("supplier-dashboard")).click();
    }

    this.clickBuyerDashboard = function () {
        element(by.id("buyer-dashboard")).click();
    }
 
}

module.exports = new Dashboard;