var Getstarted = require(__dirname + '/../../getstarted/getstarted.page.js');
var Homepagelogin = require(__dirname + '/../../homepagelogin/homepagelogin.page.js');
var Dashboard = require(__dirname + '/../dashboard.page.js');
var Buyerdashboard = require(__dirname + '/buyer-dashboard.page.js');
var Home=require(__dirname+'/../../homepage/homepage.page.js');

describe("Buyers Dashboard Functionality", function () {

    it("Login into Buyers Dashboard", function () {
        Getstarted.get();
    })

    it("Enter Buyers Email", function () {
        Homepagelogin.userName('livelikeme1986+buyer27072018@gmail.com');
    })

    it("Enter Buyers Password", function () {
        Homepagelogin.userPwd('supplierscave');
    })

    it("Click on Login", function () {
        Homepagelogin.userLogin();
    })

    it("clicks on actions in application", function () {
        Dashboard.clickActions();
    })

    it("Click on Buyers Dashboard", function () {
        Dashboard.clickBuyerDashboard();
    })

    it("Click on Shorlists in Buyer Dashboard", function () {
        Buyerdashboard.clickShorlists();
    })

    it("Click on Messages in Buyer Dashboard ", function () {
        Buyerdashboard.clickMessages();
    })

    it("Click on Suppliers Logo",function(){
        Buyerdashboard.clickOnSiteLogo();
    })

    it("Logout form Buyer Dashboard",function(){
        Dashboard.clickLogout();
    })
})