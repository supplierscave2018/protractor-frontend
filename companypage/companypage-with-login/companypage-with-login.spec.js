var Getstarted=require(__dirname+'/../../getstarted/getstarted.page.js');
var Homepagelogin=require(__dirname+'/../../homepagelogin/homepagelogin.page.js');
var Homepage=require(__dirname+'/../../homepage/homepage.page.js');
var Companypagewithlogin=require(__dirname+'/companypage-with-login.page.js');
var Directory=require(__dirname+'/../../directory/directory.page.js');

describe("Company Page Functionality",function(){
    
    it("Open Suppliercave Application",function(){
        Getstarted.get();
    })

    it("Enter UserName",function(){
        Homepagelogin.userName('livelikeme1986+supplieradmin8@gmail.com');
    })

    it("Enter Password",function(){
        Homepagelogin.userPwd('supplierscave');
    })

    it("Click on Login",function(){
        Homepagelogin.userLogin();
    })
    
    it("Click on Companypage",function(){
        Homepage.clickOnDirectory();
    })

    it("Click on Instruments",function(){
        Directory.clickOnPipe();
    })

    it("Click on UPC INSTRUMENTS PVT LTD",function(){
        Directory.clickOnStainLessIndia();
    })

    it("Click on Company Detials",function(){
        Companypagewithlogin.clickOnCompanyDetials();
    })


    it("Click On Home in Company Page", function () {
        Companypagewithlogin.clickOnHome();
    })

    it("Click on AboutUs in Compage page", function () {
        Companypagewithlogin.clickOnAboutUs();
    })

    it("Click on Products in Company page", function () {
        Companypagewithlogin.clickOnProducts();
    })

    it("Click on View more Products",function(){
        Companypagewithlogin.clickOnViewMoreProducts();
    })

    it("Click on Back ",function(){
        Companypagewithlogin.backViewMoreProducts();
    })

    it("Click on Contack in Company page", function () {
        Companypagewithlogin.clickOnContact();
    })

    it("Enter Message in Contact Supplier", function () {
        Companypagewithlogin.enterMessageInContactSupplier('More Information I want to Know about the Product');
    })

    it("Upload contact supplier",function(){
        Companypagewithlogin.uploadData();
    })

    it("Submit Contact Supplier Information",function(){
        Companypagewithlogin.clickOnSubmitInContactSupplier();

    })
    
})