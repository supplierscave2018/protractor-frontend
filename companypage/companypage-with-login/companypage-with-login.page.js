var Companypagewithlogin=function(){

    this.clickOnCompanyDetials = function () {
        element(by.id('company-intro')).click();
    }
   
    this.clickOnHome = function () {
        element(by.className('fa fa-home')).click();
    }

    this.clickOnAboutUs = function () {
        element(by.className('fa fa-user')).click();
    }

    this.clickOnProducts = function () {
        element(by.className('fa fa-cart-plus')).click();
    }

    this.clickOnViewMoreProducts=function(){
        element(by.id('view-more-products')).click();
    }

    this.backViewMoreProducts=function(){
        element(by.id('view-all-products-back')).click();
    }

    this.viewDetials=function(){
        element(by.id('view-detials')).click();
    }

    this.clickOnContact = function () {
        element(by.className('fa fa-phone-square')).click();
    }

    this.clickOnFlanges = function () {
        element(by.xpath('//*[@id="hierarchical-categories"]/div/div/div[2]/div/div[2]/div/a')).click();
    }

    this.enterNameInContactSupplier = function (data) {
        element(by.model('enquiryMessage.sender_name')).sendKeys(data);
    }

    this.enterContactNumberInContactSupplier = function (data) {
        element(by.model('enquiryMessage.sender_mobile')).sendKeys(data);
    }

    this.enterEmailInContactSupplier = function (data) {
        element(by.model('enquiryMessage.sender_email')).sendKeys(data);
    }

    this.enterMessageInContactSupplier = function (data) {
        browser.sleep(2000);
        element(by.model('enquiryMessage.message')).sendKeys(data);
    }



    this.uploadData = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/companypage/Energy_1920.jpg';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(2000);
        element(by.id('choose-file-contact-supplier')).sendKeys(absolutePath); 
        browser.sleep(4000);
    };

    this.clickOnSubmitInContactSupplier = function () {
        element(by.css('[ng-click="sendMessage(enquiryMessage)"]')).click();
    }

}


module.exports=new Companypagewithlogin;