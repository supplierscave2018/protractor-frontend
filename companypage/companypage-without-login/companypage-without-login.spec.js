var Getstarted=require(__dirname+'/../../getstarted/getstarted.page.js');
var Homepage=require(__dirname+'/../../homepage/homepage.page.js');
var Companypagewithoutlogin=require(__dirname+'/companypage-without-login.page.js');
var Directory=require(__dirname+'/../../directory/directory.page.js');

describe("Company Page Functionality",function(){
    
    it("Open Suppliercave Application",function(){
        Getstarted.get();
    })
    
    it("Click on Companypage",function(){
        Homepage.clickOnDirectory();
    })

    it("Click on Instruments",function(){
        Directory.clickonInstrument();
    })

    it("Click on UPC INSTRUMENTS PVT LTD",function(){
        Directory.clickOnUpcInstrumentsPvtLtd();
    })

    it("Click on Company Detials",function(){
        Companypagewithoutlogin.clickOnCompanyDetials();
    })


    it("Click On Home in Company Page", function () {
        Companypagewithoutlogin.clickOnHome();
    })

    it("Click on AboutUs in Compage page", function () {
        Companypagewithoutlogin.clickOnAboutUs();
    })

    it("Click on Products in Company page", function () {
        Companypagewithoutlogin.clickOnProducts();
    })

    it("Click on View more Products",function(){
        Companypagewithoutlogin.clickOnViewMoreProducts();
    })

    it("Click on Back ",function(){
        Companypagewithoutlogin.backViewMoreProducts();
    })

    // it("Click on View Detials of the product",function(){
    //     Companypage.viewDetials();
    // })

    it("Click on Contack in Company page", function () {
        Companypagewithoutlogin.clickOnContact();
    })

    it("Enter Name in Contact Supplier", function () {
        Companypagewithoutlogin.enterNameInContactSupplier('Rajendra Prasad');
    })

    it("Enter Contact Number in Contact Supplier", function () {
        Companypagewithoutlogin.enterContactNumberInContactSupplier('8296352145');
    })

    it("Enter Email in Contact Supplier", function () {
        Companypagewithoutlogin.enterEmailInContactSupplier('livelikeme1986@gmail.com');
    })


    it("Enter Message in Contact Supplier", function () {
        Companypagewithoutlogin.enterMessageInContactSupplier('More Information I want to Know about the Product');
    })

    it("Upload contact supplier",function(){
        Companypagewithoutlogin.uploadData();
    })

    it("Submit Contact Supplier Information",function(){
        // Element(by.id('contact-supplier-submit')).click();
        Companypagewithoutlogin.clickOnSubmitInContactSupplier();

    })
    
})