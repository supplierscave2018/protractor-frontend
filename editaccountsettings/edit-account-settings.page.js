var Editaccountsettings = function () {

    this.editProfile = function () {
        element(by.className('edit-account')).click();
    }

    this.editBuyerCheckbox = function () {
        $('[id="checkbox1"]').click();
    }

    this.editSupplierCheckbox = function () {
        $('[id="checkbox2"]').click();
    }

    this.clearAddress = function () {
        var foo = element(by.model('current_user.data.address'));
        expect(foo.getAttribute('ng-model')).toEqual('current_user.data.address');
        foo.clear();
    }

    this.editAddress = function (data) {
        element(by.model('current_user.data.address')).sendKeys(data);
    }

    this.clearCity = function () {
        var foo = element(by.model('current_user.data.location_city'));
        expect(foo.getAttribute('ng-model')).toEqual('current_user.data.location_city');
        foo.clear();
    }


    this.editCity = function (data) {
        element(by.model('current_user.data.location_city')).sendKeys(data);
    }

    this.clearCountry = function () {
        var foo = element(by.model('current_user.data.location_country'));
        expect(foo.getAttribute('ng-model')).toEqual('current_user.data.location_country');
        foo.clear();
    }

    this.editCountry = function (data) {
        element(by.model('current_user.data.location_country')).sendKeys(data);
    }

    this.clearCountryCode = function () {
        var foo = element(by.model('current_user.data.country_code'));
        expect(foo.getAttribute('ng-model')).toEqual('current_user.data.country_code');
        foo.clear();
    }

    this.editCountryCode = function (data) {
        element(by.model('current_user.data.country_code')).sendKeys(data);
    }

    this.clearContactNumber=function(){
        var foo=element(by.model('current_user.data.contact_no'));
        expect(foo.getAttribute('ng-model')).toEqual('current_user.data.contact_no');
        foo.clear();   
    }

    this.editContactNumber = function (data) {
        element(by.model('current_user.data.contact_no')).sendKeys(data);
    }

    this.clearEmail = function () {
        var foo = element(by.model('current_user.data.email'));
        expect(foo.getAttribute('ng-model')).toEqual('current_user.data.email');
        foo.clear();
    }

    this.editEmail = function (data) {
        element(by.model('current_user.data.email')).sendKeys(data);
    }

    this.clearFirstName=function(){
        var foo=element(by.model('current_user.data.first_name'));
        expect(foo.getAttribute('ng-model')).toEqual('current_user.data.first_name');
        foo.clear();
    }

    this.editFirstName = function (data) {
        element(by.model('current_user.data.first_name')).sendKeys(data);
    }

    this.clearLastName=function(){
        var foo=element(by.model('current_user.data.last_name'));
        expect(foo.getAttribute('ng-model')).toEqual('current_user.data.last_name');
        foo.clear();
    }

    this.editLastName = function (data) {
        element(by.model('current_user.data.last_name')).sendKeys(data);
    }

    this.clearDesignation=function(){
        var foo= element(by.model('current_user.data.designation'));
        expect(foo.getAttribute('ng-model')).toEqual('current_user.data.designation');
        foo.clear();
    }


    this.editDesignation = function (data) {
        element(by.model('current_user.data.designation')).sendKeys(data);
    }

    this.clickOnSiteLogo=function(){
        element(by.id('site-logo')).click();
    }

    this.clickOnsave = function (data) {
        element(by.id('save-profile')).click();
        // browser.sleep(5000);
    }


}
module.exports = new Editaccountsettings;