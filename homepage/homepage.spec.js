var Home = require('../homepage/homepage.page.js');
var Getstarted = require('../getstarted/getstarted.page.js');
var Homepagelogin = require('../homepagelogin/homepagelogin.page.js');
var Dashboard = require('../dashboard/dashboard.page.js');

describe('Homepage', function () {


    it("Opens homepage for Supplierscave Application ", function () {
        Getstarted.get();
    });

    it("Click on Directory in Home Page", function () {
        Home.clickOnDirectory();
    })

    it("Click on Supplierscave Icon widget-logo", function () {
        Home.clickOnwidgetLogo();
    })
    
    it("Click on Stock-And-Inventory in Home Page", function () {
        Home.clickOnStockAndInventory();
    })

    it("Click on Supplierscave Icon widget-logo", function () {
        Home.clickOnwidgetLogo();
    })

    it("Click on Rental in Home Page", function () {
        Home.clickOnRental();
    })

    it("Click on Supplierscave Icon Site logo", function () {
        Home.clickOnSiteLogo();
    })

    it("Subscribe for Updates", function () {
        Home.subscribeForUpdates('livelikeme1986@gmail.com');
    })

    it("Click on Submit for subcribing for updates", function () {
        Home.clickOnSubscribeForUpdates();
    })

    // it("Click on SuppliersCave Icon Site-Log", function () {
    //     Home.clickOnSiteLogo();
    // })

    it("Click on Companies in homepage it will open list of companies",function(){
        Home.clickOnCompaniesInHomePge();
    })

    it("click on Supplierscave Icon Site Logo",function(){
        Home.clickOnSiteLogo();
    })

    it("Click on Products in homepage it will open list of products",function(){
        Home.clickOnDirectoryInHomepage();
    })

   
    it("click on Supplierscave Icon Site Logo",function(){
        Home.clickOnSiteLogo();
    })

    it("Click on Inventory in homepage it will open list of inventories",function(){
        Home.clickOnInventoryInHomePage();
    })

    it("click on Supplierscave Icon Site Logo",function(){
        Home.clickOnSiteLogo();
    })

    it("Click on Rental in homepage it will open list of rental",function(){
        Home.clickOnRentalInHomePage();
    })

    it("click on Supplierscave Icon Site Logo",function(){
        Home.clickOnSiteLogo();
    })








    // it("AboutUs information of Supplierscave Application", function () {
    //     Home.clickAboutUs();
    // });

    // it("SuppliersCave Application how it works Information", function () {
    //     Home.clickHowItWorks();
    // });

    // it("Contacts us", function () {
    //     Home.clickContactUs();
    // })

    // it("Frequently Asked Questiongs", function () {
    //     Home.clickFrequentlyAskedQuestions();
    // })

    // it("Click on Privacy Policy in Home Page", function () {
    //     Home.clickOnPrivacyPolicy();
    // })

    // it("Click on Terms and Conditions in Home Page", function () {
    //     Home.clickOnTermsAndConditions();
    // })

    // it("Click on Supplierscave Icon widget-logo", function () {
    //     Home.clickOnSiteLogo();
    // })

    // it("Click on facebook Logo, It will navigate to Suppliers Cave facebook page", function () {
    //     Home.clickOnFacebookLogo();
    // })

    // it("Click on Google Plus it  navigates to google plus page", function () {
    //     Home.clickOnGooglePlusHeaderLogo();
    // })

    // it("Click on Linkedin it navigates Linkedin page",function(){
    //     Home.clickonLinkedInHeaderLogo();
    // })

});