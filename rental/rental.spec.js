var Rental = require(__dirname+'/rental.page.js');
var Getstarted = require(__dirname+'/../getstarted/getstarted.page.js');
var Home = require(__dirname+'/../homepage/homepage.page.js');

describe("Rental Functionality",function(){
    it("Opens SuppliersCave Application",function(){
        Getstarted.get();
    })

    it("Click on Rental in Homepage",function(){
        Home.clickOnRental();
    })

    it("Click on Suppliers Logo",function(){
        Home.clickOnSiteLogo();
    })
})