var Forgotpassword = require(__dirname + '/forgot-password.page.js');
var Getstarted = require(__dirname + '/../getstarted/getstarted.page.js');
var Homepage = require(__dirname + '/../homepage/homepage.page.js')

describe("Forgot Password", function () {

    it("Opens supplier cave application", function () {
        Getstarted.get();
    })

    it("Click on Forgot Password in Home page", function () {
        Homepage.clickOnForgotPassword();
    })

    it("Enter Email Address to reset your password",function(){
        Forgotpassword.enterEmialId('livelikeme1986+buyer24072018@gmail.com');
    })

    it("Click on submit after entering Email Address",function(){
        Forgotpassword.forgotPasswordSubmit();
    })

    it("Opens the pop-up window to enter the new password",function(){
        Forgotpassword.get();
    })

    it("Enter New password which you want to update",function(){
        Forgotpassword.enterPassword('supplierscave2');
    })

    it("Re-Enter New password which you want to update",function(){
        Forgotpassword.reEnterPassword('supplierscave2');
    })

    it("Click on Reset Password",function(){
        Forgotpassword.clickOnResetPassword();
    })

})