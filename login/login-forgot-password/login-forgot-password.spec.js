var Getstarted = require(__dirname + '/../../getstarted/getstarted.page.js');
var Forgotpassword = require(__dirname + '/../../forgotpassword/forgot-password.page.js');
var Loginpageforgotpassword = require(__dirname + '/login-forgot-password.page.js');


describe("Login-Page Forgot-Password Functinality", function () {

    it("Opens Supplierscave Application", function () {
        Getstarted.loginGet();
    })

    it("Click on Forgot Password", function () {
        Loginpageforgotpassword.clickOnLoginPageForgotPassword();
    })

    it("Enter Email to reset your password", function () {
        Forgotpassword.enterEmialId("livelikeme1986+buyer27072018@gmail.com");
    })

    it("Click on Submit after entering email address",function(){
        Loginpageforgotpassword.loginPageForgotPassword();
    })

    it("Opens the pop-up window to enter the new password",function(){
        Forgotpassword.get();
    })

    it("Enter New password which you want to update",function(){
        Forgotpassword.enterPassword('supplierscave3');
    })

    it("Re-Enter New password which you want to update",function(){
        Forgotpassword.reEnterPassword('supplierscave3');
    })

    it("Click on Reset Password",function(){
        Forgotpassword.clickOnResetPassword();
    })



})