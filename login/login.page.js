var Login = function () {

    this.enterLoignUserName = function (data) {
        element(by.id('login-username')).sendKeys(data);
    }

    this.enterLoginUserPwd = function (data) {
        element(by.id('login-password')).sendKeys(data);
    }

    this.clickLogin = function () {
        element(by.id('login-submit')).click();
        // browser.sleep(5000);
    }

    this.clickAdminDashboard = function () {
        element(by.id('current-user-admin-dashboard')).click();
    }

    this.clickSupplierDashboard = function () {
        element(by.id('supplier-dashboard-inventory')).click();
        browser.sleep(3000);
    }

    this.clickBuyerDashboard = function () {
        element(by.id('buyer-dashboard-shortlist')).click();

    }

    this.clickOnLogout = function () {
        element(by.id('current-user-logout')).click();
    }

    this.clickOnForgotPassword = function () {
        element(by.id('login-forgot-password')).click();
    }

}
module.exports = new Login;