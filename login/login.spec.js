var Getstarted = require(__dirname + '/../getstarted/getstarted.page.js')
var Userlogin = require(__dirname + '/login.page.js');
var Dashboard = require(__dirname + '/../dashboard/dashboard.page.js');


describe("Login Functionality", function () {
    it("Opens Login page", function () {
        Getstarted.loginGet();
    })

    // it("Click On Forgot Password",function(){
    //     element(by.id('login-forgot-password')).click();
    // })

    it("Enter Login UserName", function () {
        Userlogin.enterLoignUserName('livelikeme1986+admin24072018700@gmail.com');
    })

    it("Enter Login Password", function () {
        Userlogin.enterLoginUserPwd('supplierscave');
    })

    it("Click on Login", function () {
        Userlogin.clickLogin();
    })

    // it("Click on Admin Dashboard", function () {
    //     Userlogin.clickAdminDashboard();
    // })

    // it("Click on Suppliers Dashboard", function () {
    //     Userlogin.clickSupplierDashboard();
    // })

    it("Click on Buyers Dashboard", function () {
        Userlogin.clickBuyerDashboard();
    })

    // it("Click on Suppliers Logo",function(){
    //     Home.clickOnSiteLogo();
    // })

    // it("Click on Logout",function(){
    //     Dashboard.clickLogout();
    // })

    // it("Click On Logout in Login Page", function () {
    //     Userlogin.clickOnLogout();
    // })

})