var Getstarted = require(__dirname + '/../getstarted/getstarted.page.js');
var Homepagelogin = require(__dirname + '/../homepagelogin/homepagelogin.page.js');
var Register = require(__dirname + '/register.page.js');
var Home=require(__dirname+'/../homepage/homepage.page.js');

describe("Register with SuppliersCave Application", function () {

    it("Register Getstarted Page", function () {
        Getstarted.get();
    })

    it("Click on New User Register",function(){
        Home.clickOnNewRegister();
    })

    it("Enter Email for new Registration",function(){
        Register.enterEmailForRegistration('livelikeme1986+admin27072018@gmail.com');
    })

    it("Enter password for new Registration",function(){
        Register.enterPwdForRegistration('supplierscave');
    })

    it("Re-Enter password for new Registration",function(){
        Register.reEnterPwdForRegistration('supplierscave');
    })

    it("Select Buyer Dashboard",function(){
        Register.selectBuyercheckbox();
    })

    it("Select Supplier Dashboard",function(){
        Register.selectSupplierCheckbox();
    })

    it("Select I agree to Terms & Conditions",function(){
        Register.selectTermsAndConditions();
    })

    it("Select Which ever Applicatble",function(){
        Register.clickOnSelectWhichEverApplicable();
    })
   
    it("Click on Register for registration",function(){
        Register.clickOnRegistration();
    })

    it("Open additional Information Browser",function(){
        Register.get();
    })  
  
})