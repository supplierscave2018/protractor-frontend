var Register = require(__dirname + '/../register/register.page.js');
var Registeradditional = require(__dirname + '/register-additional-information.page.js');

describe("Additional Information while registering", function () {

    it("Enter First Name", function () {
        Registeradditional.clickOnFirstName('Admin-27072018');
    })

    it("Enter Last Name", function () {
        Registeradditional.clickOnLastName('Sreekanth');
    })

    it("Enter Country code", function () {
        Registeradditional.clickOnCountryCode('91');
    })

    it("Enter Contact Number", function () {
        Registeradditional.clickOnContactNumber('8919714657');
    })

    it("Enter Designation", function () {
        Registeradditional.clickOnDesignation('Developer');
    })

    it("Enter Address", function () {
        Registeradditional.clickOnAddress('D-No 6-1136');
    })

    it("Enter City", function () {
        Registeradditional.clickOnCity('Chennai');
    })

    it("Enter Country", function () {
        Registeradditional.clickOnCountry('India');
    })

    it("Supplier Admin Name", function () {
        Registeradditional.supplierName('JUPITER');
    })

    it("Submit Additional update user", function () {
        Registeradditional.clickOnSubmit();
    })

})