Registeradditionalinformation=function(){

      this.clickOnFirstName=function(data){
        element(by.model('update.first_name')).sendKeys(data);
    }

    this.clickOnLastName=function(data){
        element(by.model('update.last_name')).sendKeys(data);
    }

    this.clickOnCountryCode=function(data){
        element(by.model('update.country_code')).sendKeys(data);
    }

    this.clickOnContactNumber=function(data){
        element(by.model('update.contact_no')).sendKeys(data);
    }
    this.clickOnDesignation=function(data){
        element(by.model('update.designation')).sendKeys(data);
    }
    this.clickOnAddress=function(data){
        element(by.model('update.address')).sendKeys(data);
    }
    this.clickOnCity=function(data){
        element(by.model('update.location_city')).sendKeys(data);
    }
    this.clickOnCountry=function(data){
        element(by.model('update.location_country')).sendKeys(data);
    }

    this.selectAreYouASupplierAdmin=function(){
        element(by.id('checkbox3')).click();
    }

    this.supplierName=function(data){
        element(by.id('update-company')).sendKeys(data);
    }

    this.clickOnSubmit=function(){
        browser.sleep(5000);
        element(by.id('update-user')).click();
        browser.sleep(5000);
    }





}
module.exports= new Registeradditionalinformation;