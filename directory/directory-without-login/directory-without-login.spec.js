var Directorywithoutlogin = require(__dirname + '/directory-without-login.page.js');
var Directory=require(__dirname+'/../directory.page.js');
var Home = require(__dirname + '/../../homepage/homepage.page.js');
var Getstarted = require(__dirname + '/../../getstarted/getstarted.page.js');

describe("Directory Functionality Without Login", function () {

    it("Opens Supplierscave Application", function () {
        Getstarted.get();
    })

    it("Click on Directory in Homepage", function () {
        Home.clickOnDirectory();
    })

    it("Click On Instruments", function () {
        Directory.clickonInstrument();
    })
    
    it("Click on UPC INSTRUMENTS PVT. LTD", function () {
        Directory.clickOnUpcInstrumentsPvtLtd();
    })

    it("Click on Send Enquiry regarding product", function () {
        Directorywithoutlogin.clickOnSendEnquiry();
    })

    it("Give Sender Email", function () {
        Directorywithoutlogin.senderEmail('power902569@gmail.com');
    })

    it("Give Sender Mobile Number", function () {
        Directorywithoutlogin.senderMobileNumber('9676902569');
    })

    it("Enquiry Subject", function () {
        Directorywithoutlogin.sendEnquirySubject('Test');
    })

    it("Enquiry Message", function () {
        Directorywithoutlogin.sendEnquiryMessage('  Test Enquiry Message');
    })

    it("Upload Send Enquiry File", function () {
        Directorywithoutlogin.uploadFiles();
    })

    it("Send Enquiry", function () {
        Directorywithoutlogin.sendEnquiry();
    })
    
})