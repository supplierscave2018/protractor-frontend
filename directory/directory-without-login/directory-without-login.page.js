var Directorywithoutlogin = function () {

    this.clickonInstrument = function () {
        element(by.xpath('//*[@id="hierarchical-categories"]/div/div/div[2]/div/div[4]/div/a')).click();
    }

    this.clickOnStainLessIndia = function () {     
        element.all(by.css('.hit-supplier a')).first().click();
    }

    this.clickOnUpcInstrumentsPvtLtd = function () {
        element.all(by.css('.hit-supplier a')).first().click();
    }

    this.clickOnSendEnquiry = function () {
        // element(by.id('product-item-send-enquiry')).click();
        // element(by.xpath('//*[@id="product-item-send-enquiry"]')).click();
        element(by.id('product-item-send-enquiry')).click();


    }

    this.senderEmail = function (data) {
        element(by.model('sender_email')).sendKeys(data);
    }

    this.senderMobileNumber = function (data) {
        element(by.model('sender_mobile')).sendKeys(data);
    }

    this.sendEnquirySubject = function (data) {
        element(by.model('send_enquiry_subject')).sendKeys(data);
    }

    this.sendEnquiryMessage = function (data) {
        element(by.id('ui-tinymce-1_ifr')).sendKeys(data);
    }

    this.uploadFiles = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-directory/test.csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        element(by.id('send-enquiry-choose-file')).sendKeys(absolutePath);
        element(by.id('send-enquiry-upload-file')).click();
    }

    this.sendEnquiry = function () {
        element(by.id('send-enquiry')).click();
    }

}
module.exports = new Directorywithoutlogin;