var Directory = function () {

    this.clickOnPipe = function () {       
        element(by.xpath('//*[@id="hierarchical-categories"]/div/div/div[2]/div/div[5]/div[1]/a')).click();
        browser.sleep(2000);
    }

    this.clickonInstrument = function () {
        browser.sleep(2000);       
        element(by.xpath('//*[@id="hierarchical-categories"]/div/div/div[2]/div/div[4]/div[1]/a')).click();
        browser.sleep(2000);
    }

    this.clickOnStainLessIndia = function () {    
        browser.sleep(1000);
        element.all(by.css('.hit-supplier a')).first().click();
    }

    this.clickOnUpcInstrumentsPvtLtd = function () {
        browser.sleep(1000);
        element.all(by.css('.hit-supplier a')).first().click();
        browser.sleep(1000);
    }

}
module.exports = new Directory;